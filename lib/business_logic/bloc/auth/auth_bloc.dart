import 'dart:async';
import 'package:motionlife/data/model/request_state.dart';
import 'package:motionlife/data/model/login/login.dart';
import 'package:motionlife/data/repository/auth/auth_model.dart';


class AuthBloc{
    late AuthModel _loginClient;
    late StreamController<RequestState<Login>> _loginController;

    StreamSink<RequestState<Login>> get loginSink => _loginController.sink;
    Stream<RequestState<Login>> get loginStream => _loginController.stream;
 
    AuthBloc() {
      _loginController = StreamController<RequestState<Login>>();
      _loginClient = AuthModel();
    }

    login(Map<String, dynamic> body) async {
      loginSink.add(RequestState.loading());
      try {
        Login data = await _loginClient.login(body: body);
        loginSink.add(RequestState.completed(data));
      } catch (e) {
        loginSink.add(RequestState.error(e.toString()));
        print(e);
      }
    }

    dispose() {
      _loginController.close();
    }
}