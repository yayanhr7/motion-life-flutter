import 'dart:async';

import 'package:dio/dio.dart';
import 'package:motionlife/business_logic/bloc/base_bloc.dart';
import 'package:motionlife/config/api/dio_client.dart';
import 'package:motionlife/data/model/bestseller/best_seller.dart';
import 'package:motionlife/data/repository/bestseller/best_seller_repository.dart';

import '../../../data/model/request_state.dart';

class BestSellerBloc implements BaseBloc{
  Dio dio = DioClient.createInstance();

  late BestSellerRespository _respository;
  final StreamController<RequestState<BestSellerModel>> _bestSellerController = StreamController<RequestState<BestSellerModel>>();


  StreamSink<RequestState<BestSellerModel>> get bestSellerSink => _bestSellerController.sink;
  Stream<RequestState<BestSellerModel>> get bestSellerStream => _bestSellerController.stream;

  BestSellerBloc(){
    _respository = BestSellerRespository(dio);
  }

  getBestSeller() async{
    bestSellerSink.add(RequestState.loading());
    try{
      BestSellerModel data = await _respository.getBestSeller();
      bestSellerSink.add(RequestState.completed(data));
    }catch(e){
      bestSellerSink.add(RequestState.error(e.toString()));
    }
  }


  @override
  void dispose() {
    _bestSellerController.close();
  }

}