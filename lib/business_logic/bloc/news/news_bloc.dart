import 'dart:async';

import 'package:dio/dio.dart';
import 'package:motionlife/business_logic/bloc/base_bloc.dart';
import 'package:motionlife/config/api/dio_client.dart';
import 'package:motionlife/data/model/promo/news_model.dart';
import 'package:motionlife/data/model/request_state.dart';
import 'package:motionlife/data/repository/news/news_repository.dart';

class NewsBloc implements BaseBloc{
  late NewsRepository _repository;
  final StreamController<RequestState<List<DataNews>>> _controller = StreamController<RequestState<List<DataNews>>>();

  Dio dio = DioClient.createInstance();
  StreamSink<RequestState<List<DataNews>>> get newsSink => _controller.sink;
  Stream<RequestState<List<DataNews>>> get newsStream => _controller.stream;

  NewsBloc(){
    _repository = NewsRepository(dio);
  }

  getNews() async{
      newsSink.add(RequestState.loading());
      try{
        List<DataNews> data = await _repository.getNews();
        newsSink.add(RequestState.completed(data));
      }catch(e){
        newsSink.add(RequestState.error(e.toString()));
      }
  }

  @override
  void dispose() {
    _controller.close();
  }
}