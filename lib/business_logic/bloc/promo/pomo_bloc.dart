import 'dart:async';

import 'package:dio/dio.dart';
import 'package:motionlife/business_logic/bloc/base_bloc.dart';
import 'package:motionlife/config/api/dio_client.dart';
import 'package:motionlife/data/model/promo/promo_model.dart';
import 'package:motionlife/data/model/request_state.dart';
import 'package:motionlife/data/repository/promo/promo_repository.dart';


class PromoBloc implements BaseBloc{
  late PromoRepository _promoRepository;
  final StreamController<RequestState<PromoModel>> _promoController = StreamController<RequestState<PromoModel>>();
  Dio dio = DioClient.createInstance();

  StreamSink<RequestState<PromoModel>> get promoSink => _promoController.sink;
  Stream<RequestState<PromoModel>> get promoStream => _promoController.stream;

  PromoBloc(){
    _promoRepository = PromoRepository(dio);
  }

  getPromo() async{
    promoSink.add(RequestState.loading());
    try{
      PromoModel data = await _promoRepository.getPromo();
      promoSink.add(RequestState.completed(data));
    }catch(e){
      promoSink.add(RequestState.error(e.toString()));
    }
  }

  @override
  dispose(){
    _promoController.close();
  }
}