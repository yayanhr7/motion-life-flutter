import 'dart:async';

import 'package:dio/dio.dart';
import 'package:motionlife/business_logic/bloc/base_bloc.dart';
import 'package:motionlife/data/model/tips/tips_model.dart';
import 'package:motionlife/data/repository/tips/tips_repository.dart';

import '../../../config/api/dio_client.dart';
import '../../../data/model/request_state.dart';

class TipsBloc implements BaseBloc{
  late TipsRepository _tipsRepository;
  final StreamController<RequestState<TipsModel>> _tipsController = StreamController<RequestState<TipsModel>>();
  Dio dio = DioClient.createInstance();

  StreamSink<RequestState<TipsModel>> get tipsSink => _tipsController.sink;
  Stream<RequestState<TipsModel>> get tipsStream => _tipsController.stream;

  TipsBloc(){
    _tipsRepository = TipsRepository(dio);
  }

  getTips() async{
    tipsSink.add(RequestState.loading());
    try{
      TipsModel data = await _tipsRepository.getTips();
      tipsSink.add(RequestState.completed(data));
    }catch(e){
      tipsSink.add(RequestState.error(e.toString()));
    }
  }

  @override
  void dispose() {
    _tipsController.close();
  }

}