import 'dart:async';

import 'package:dio/dio.dart';
import 'package:motionlife/business_logic/bloc/base_bloc.dart';
import 'package:motionlife/data/model/youtube/youtube_model.dart';
import 'package:motionlife/data/repository/youtube/youtube_repository.dart';

import '../../../config/api/dio_client.dart';
import '../../../data/model/request_state.dart';

class YoutubeBloc implements BaseBloc{
  late YoutubeRepository _repository;
  final StreamController<RequestState<YoutubeModel>> _tipsController = StreamController<RequestState<YoutubeModel>>();
  Dio dio = DioClient.createInstance();

  StreamSink<RequestState<YoutubeModel>> get youtubeSink => _tipsController.sink;
  Stream<RequestState<YoutubeModel>> get youtubeStream => _tipsController.stream;

  YoutubeBloc(){
    _repository = YoutubeRepository(dio);
  }

  getYoutube() async{
    youtubeSink.add(RequestState.loading());
    try{
      YoutubeModel data = await _repository.getYoutube();
      youtubeSink.add(RequestState.completed(data));
    }catch(e){
      youtubeSink.add(RequestState.error(e.toString()));
    }
  }

  @override
  void dispose() {
    _tipsController.close();
  }
}