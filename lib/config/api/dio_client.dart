import 'package:dio/dio.dart';

import 'endpoint.dart';


class DioClient {

  DioClient._privateConstructor();

  static final DioClient _instance = DioClient._privateConstructor();

  static DioClient get instance => _instance;

  static Dio createInstance() {
    BaseOptions options = BaseOptions(
        baseUrl: Endpoint.baseUrl,
        connectTimeout: 60000,
        receiveTimeout: 60000,
        headers: {
          'Accept': '*/*',
          'Content-Type': 'multipart/form-data',
        });

    var dio = Dio(options);
    dio.interceptors.add(LogInterceptor(request: true, requestHeader: true, requestBody: true, responseBody: true, responseHeader: true, error: true));
    return dio;
  }
}
