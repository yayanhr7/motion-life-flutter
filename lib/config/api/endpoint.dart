class Endpoint{
  static const baseUrl = "https://uat-mhario.mnclife.com/";
  static const loginUrl = "x-accountSignIn.php";
  static const cmsUrl = "/cms-api/";
  static const promoUrl = cmsUrl + "v1/promo";
  static const kabarHario = cmsUrl + "/v1/kabarhario";
  static const bestSellerUrl = cmsUrl + "/v1/packages/best-seller";
  static const tipsUrl = cmsUrl + "/contents/tips?webkey=hario&get=paginate&page_type=1";
  static const youtubeUrl = cmsUrl + "/contents/events?webkey=hario&get=paginate&with=metas";
}