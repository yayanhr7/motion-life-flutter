import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:motionlife/presentation/pages/dashboard/dashboard.dart';

import '../../presentation/login/login_view.dart';
import '../../presentation/onboard/onboard.dart';

class AppRouter {
  static const loginRoute = '/';
  static const onboard = '/onboard';
  static const signUpRoute = '/signUp';
  static const verifyPassword = '/verifyPassword';
  static const dashboard= '/dashboard';
  static const noInternet = '/noInternet';

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case loginRoute:
        return MaterialPageRoute(builder: (_) => LoginView());
      case onboard:
        return MaterialPageRoute(builder: (_) => OnBoard());
      case dashboard:
        return MaterialPageRoute(builder: (_) => Dashboard());
      default:
        return MaterialPageRoute(builder: (_) => const Text("No route found"));
    }
  }
}
