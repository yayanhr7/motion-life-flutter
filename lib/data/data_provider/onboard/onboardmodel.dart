class OnboardModel{
  String image;
  OnboardModel(this.image);
}

List<OnboardModel> getOnBoardData(){

  OnboardModel onboard1 = OnboardModel("assets/images/onboard1.png");
  OnboardModel onboard2 = OnboardModel("assets/images/onboard2.png");
  OnboardModel onboard3 = OnboardModel("assets/images/onboard3.png");
  
  List<OnboardModel> list = <OnboardModel>[];
  list.add(onboard1);
  list.add(onboard2);
  list.add(onboard3);

  return list;
}