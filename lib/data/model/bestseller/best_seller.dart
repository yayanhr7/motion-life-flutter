class BestSellerModel {
  int? currentPage;
  List<DataBestSeller>? data;
  String? firstPageUrl;
  int? from;
  int? lastPage;
  String? lastPageUrl;
  String? nextPageUrl;
  String? path;
  int? perPage;
  String? prevPageUrl;
  int? to;
  int? total;

  BestSellerModel(
      {this.currentPage,
        this.data,
        this.firstPageUrl,
        this.from,
        this.lastPage,
        this.lastPageUrl,
        this.nextPageUrl,
        this.path,
        this.perPage,
        this.prevPageUrl,
        this.to,
        this.total});

  BestSellerModel.fromJson(Map<String, dynamic> json) {
    currentPage = json['current_page'];
    if (json['data'] != null) {
      data = <DataBestSeller>[];
      json['data'].forEach((v) {
        data!.add(new DataBestSeller.fromJson(v));
      });
    }
    firstPageUrl = json['first_page_url'];
    from = json['from'];
    lastPage = json['last_page'];
    lastPageUrl = json['last_page_url'];
    nextPageUrl = json['next_page_url'];
    path = json['path'];
    perPage = json['per_page'];
    prevPageUrl = json['prev_page_url'];
    to = json['to'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['current_page'] = this.currentPage;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['first_page_url'] = this.firstPageUrl;
    data['from'] = this.from;
    data['last_page'] = this.lastPage;
    data['last_page_url'] = this.lastPageUrl;
    data['next_page_url'] = this.nextPageUrl;
    data['path'] = this.path;
    data['per_page'] = this.perPage;
    data['prev_page_url'] = this.prevPageUrl;
    data['to'] = this.to;
    data['total'] = this.total;
    return data;
  }
}

class DataBestSeller {
  int? id;
  int? categoryId;
  int? productId;
  String? categoryName;
  String? productName;
  String? packageName;
  String? packageCode;
  int? termOfPaymentId;
  String? termOfPaymentDesc;
  int? isBestseller;
  String? image;
  int? isActive;

  DataBestSeller(
      {this.id,
        this.categoryId,
        this.productId,
        this.categoryName,
        this.productName,
        this.packageName,
        this.packageCode,
        this.termOfPaymentId,
        this.termOfPaymentDesc,
        this.isBestseller,
        this.image,
        this.isActive});

  DataBestSeller.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    categoryId = json['category_id'];
    productId = json['product_id'];
    categoryName = json['category_name'];
    productName = json['product_name'];
    packageName = json['package_name'];
    packageCode = json['package_code'];
    termOfPaymentId = json['term_of_payment_id'];
    termOfPaymentDesc = json['term_of_payment_desc'];
    isBestseller = json['is_bestseller'];
    image = json['image'];
    isActive = json['is_active'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['category_id'] = this.categoryId;
    data['product_id'] = this.productId;
    data['category_name'] = this.categoryName;
    data['product_name'] = this.productName;
    data['package_name'] = this.packageName;
    data['package_code'] = this.packageCode;
    data['term_of_payment_id'] = this.termOfPaymentId;
    data['term_of_payment_desc'] = this.termOfPaymentDesc;
    data['is_bestseller'] = this.isBestseller;
    data['image'] = this.image;
    data['is_active'] = this.isActive;
    return data;
  }
}
