import 'dart:convert';

Login loginFromJson(String str) => Login.fromJson(json.decode(str));

String loginToJson(Login data) => json.encode(data.toJson());

class Login {

  String statusCode;
  String desc;
  String lastRequestOtpTime;
  List<DataLogin> data;

  Login({
    required this.statusCode,
    required this.desc,
    required this.lastRequestOtpTime,
    required this.data,
  });


  factory Login.fromJson(Map<String, dynamic> json) => Login(
    statusCode: json["STATUS_CODE"],
    desc: json["DESC"],
    lastRequestOtpTime: json["LAST_REQUEST_OTP_TIME"],
    data: List<DataLogin>.from(json["DATA"].map((x) => DataLogin.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "STATUS_CODE": statusCode,
    "DESC": desc,
    "LAST_REQUEST_OTP_TIME": lastRequestOtpTime,
    "DATA": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class DataLogin {

  String userId;
  String username;
  String email;
  String fullname;
  String phoneNumber;
  String dob;
  String gender;
  String linkImage;
  String ctoken;

  DataLogin({
    required this.userId,
    required this.username,
    required this.email,
    required this.fullname,
    required this.phoneNumber,
    required this.dob,
    required this.gender,
    required this.linkImage,
    required this.ctoken,
  });

  factory DataLogin.fromJson(Map<String, dynamic> json) => DataLogin(
    userId: json["USER_ID"],
    username: json["USERNAME"],
    email: json["EMAIL"],
    fullname: json["FULLNAME"],
    phoneNumber: json["PHONE_NUMBER"],
    dob: json["DOB"],
    gender: json["GENDER"],
    linkImage: json["LINK_IMAGE"],
    ctoken: json["CTOKEN"],
  );

  Map<String, dynamic> toJson() => {
    "USER_ID": userId,
    "USERNAME": username,
    "EMAIL": email,
    "FULLNAME": fullname,
    "PHONE_NUMBER": phoneNumber,
    "DOB": dob,
    "GENDER": gender,
    "LINK_IMAGE": linkImage,
    "CTOKEN": ctoken,
  };
}
