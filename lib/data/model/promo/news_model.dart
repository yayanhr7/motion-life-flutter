class DataNews {
  int? id;
  String? title;
  String? slug;
  String? content;
  String? image;
  String? imageWebview;
  int? isActive;

  DataNews(
      {this.id,
        this.title,
        this.slug,
        this.content,
        this.image,
        this.imageWebview,
        this.isActive});

  DataNews.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    slug = json['slug'];
    content = json['content'];
    image = json['image'];
    imageWebview = json['image_webview'];
    isActive = json['is_active'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['slug'] = this.slug;
    data['content'] = this.content;
    data['image'] = this.image;
    data['image_webview'] = this.imageWebview;
    data['is_active'] = this.isActive;
    return data;
  }
}
