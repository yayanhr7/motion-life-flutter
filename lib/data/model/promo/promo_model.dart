import 'dart:convert';

class PromoModel {
  int? currentPage;
  List<DataPromo>? data;
  String? firstPageUrl;
  int? from;
  int? lastPage;
  String? lastPageUrl;
  int? nextPageUrl;
  String? path;
  int? perPage;
  int? prevPageUrl;
  int? to;
  int? total;

  PromoModel(
      {this.currentPage,
        this.data,
        this.firstPageUrl,
        this.from,
        this.lastPage,
        this.lastPageUrl,
        this.nextPageUrl,
        this.path,
        this.perPage,
        this.prevPageUrl,
        this.to,
        this.total});

  PromoModel.fromJson(Map<String, dynamic> json) {
    currentPage = json['current_page'];
    if (json['data'] != null) {
      data = <DataPromo>[];
      json['data'].forEach((v) {
        data!.add(new DataPromo.fromJson(v));
      });
    }
    firstPageUrl = json['first_page_url'];
    from = json['from'];
    lastPage = json['last_page'];
    lastPageUrl = json['last_page_url'];
    nextPageUrl = json['next_page_url'];
    path = json['path'];
    perPage = json['per_page'];
    prevPageUrl = json['prev_page_url'];
    to = json['to'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['current_page'] = this.currentPage;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['first_page_url'] = this.firstPageUrl;
    data['from'] = this.from;
    data['last_page'] = this.lastPage;
    data['last_page_url'] = this.lastPageUrl;
    data['next_page_url'] = this.nextPageUrl;
    data['path'] = this.path;
    data['per_page'] = this.perPage;
    data['prev_page_url'] = this.prevPageUrl;
    data['to'] = this.to;
    data['total'] = this.total;
    return data;
  }
}

class DataPromo {
  int? id;
  String? type;
  String? title;
  Null? titleBar;
  String? content;
  String? image;
  Null? imageDetail;
  String? termsConditions;
  String? code;
  int? quota;
  String? amountType;
  Null? amount;
  int? amountMax;
  String? money;
  String? tglMulai;
  String? tglAkhir;
  int? isActive;
  String? nonPromo;
  String? allProduct;
  List<Products>? products;

  DataPromo(
      {this.id,
        this.type,
        this.title,
        this.titleBar,
        this.content,
        this.image,
        this.imageDetail,
        this.termsConditions,
        this.code,
        this.quota,
        this.amountType,
        this.amount,
        this.amountMax,
        this.money,
        this.tglMulai,
        this.tglAkhir,
        this.isActive,
        this.nonPromo,
        this.allProduct,
        this.products});

  DataPromo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    type = json['type'];
    title = json['title'];
    titleBar = json['title_bar'];
    content = json['content'];
    image = json['image'];
    imageDetail = json['image_detail'];
    termsConditions = json['terms_conditions'];
    code = json['code'];
    quota = json['quota'];
    amountType = json['amount_type'];
    amount = json['amount'];
    amountMax = json['amount_max'];
    money = json['money'];
    tglMulai = json['tgl_mulai'];
    tglAkhir = json['tgl_akhir'];
    isActive = json['is_active'];
    nonPromo = json['non_promo'];
    allProduct = json['all_product'];
    if (json['products'] != null) {
      products = <Products>[];
      json['products'].forEach((v) {
        products!.add(new Products.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['type'] = this.type;
    data['title'] = this.title;
    data['title_bar'] = this.titleBar;
    data['content'] = this.content;
    data['image'] = this.image;
    data['image_detail'] = this.imageDetail;
    data['terms_conditions'] = this.termsConditions;
    data['code'] = this.code;
    data['quota'] = this.quota;
    data['amount_type'] = this.amountType;
    data['amount'] = this.amount;
    data['amount_max'] = this.amountMax;
    data['money'] = this.money;
    data['tgl_mulai'] = this.tglMulai;
    data['tgl_akhir'] = this.tglAkhir;
    data['is_active'] = this.isActive;
    data['non_promo'] = this.nonPromo;
    data['all_product'] = this.allProduct;
    if (this.products != null) {
      data['products'] = this.products!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Products {
  int? productId;
  String? itemTitle;
  String? productImage;
  String? packageId;
  int? isHighlight;

  Products(
      {this.productId,
        this.itemTitle,
        this.productImage,
        this.packageId,
        this.isHighlight});

  Products.fromJson(Map<String, dynamic> json) {
    productId = json['product_id'];
    itemTitle = json['item_title '];
    productImage = json['product_image'];
    packageId = json['package_id'];
    isHighlight = json['is_highlight'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['product_id'] = this.productId;
    data['item_title '] = this.itemTitle;
    data['product_image'] = this.productImage;
    data['package_id'] = this.packageId;
    data['is_highlight'] = this.isHighlight;
    return data;
  }
}

