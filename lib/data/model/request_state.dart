class RequestState<T> {

  Status status;
  late T data;
  late String message;

  RequestState.loading() : status = Status.LOADING;

  RequestState.completed(this.data) : status = Status.COMPLETED;

  RequestState.error(this.message) : status = Status.ERROR;

}

enum Status { LOADING, COMPLETED, ERROR }