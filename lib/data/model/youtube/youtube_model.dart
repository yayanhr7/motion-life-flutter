class YoutubeModel {
  int? currentPage;
  List<DataYoutube>? data;
  String? firstPageUrl;
  int? from;
  int? lastPage;
  String? lastPageUrl;
  String? nextPageUrl;
  String? path;
  int? perPage;
  String? prevPageUrl;
  int? to;
  int? total;

  YoutubeModel(
      {this.currentPage,
        this.data,
        this.firstPageUrl,
        this.from,
        this.lastPage,
        this.lastPageUrl,
        this.nextPageUrl,
        this.path,
        this.perPage,
        this.prevPageUrl,
        this.to,
        this.total});

  YoutubeModel.fromJson(Map<String, dynamic> json) {
    currentPage = json['current_page'];
    if (json['data'] != null) {
      data = <DataYoutube>[];
      json['data'].forEach((v) {
        data!.add(new DataYoutube.fromJson(v));
      });
    }
    firstPageUrl = json['first_page_url'];
    from = json['from'];
    lastPage = json['last_page'];
    lastPageUrl = json['last_page_url'];
    nextPageUrl = json['next_page_url'];
    path = json['path'];
    perPage = json['per_page'];
    prevPageUrl = json['prev_page_url'];
    to = json['to'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['current_page'] = this.currentPage;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['first_page_url'] = this.firstPageUrl;
    data['from'] = this.from;
    data['last_page'] = this.lastPage;
    data['last_page_url'] = this.lastPageUrl;
    data['next_page_url'] = this.nextPageUrl;
    data['path'] = this.path;
    data['per_page'] = this.perPage;
    data['prev_page_url'] = this.prevPageUrl;
    data['to'] = this.to;
    data['total'] = this.total;
    return data;
  }
}

class DataYoutube {
  int? id;
  int? websiteId;
  int? contentTypeId;
  String? type;
  String? title;
  String? titleEn;
  String? titleBar;
  String? subtitle;
  String? subtitleEn;
  String? slug;
  String? excerpt;
  String? excerptEn;
  String? content;
  String? contentEn;
  String? image;
  String? imageWebview;
  String? order;
  String? seoTitle;
  String? seoKeyword;
  String? seoDescription;
  int? parentId;
  int? isActive;
  int? createdBy;
  String? createdAt;
  String? modified;
  int? viewed;
  int? itemId;
  int? categoryId;
  int? typeId;
  String? url;
  String? code;
  int? quota;
  String? amountType;
  String? amount;
  String? amountMax;
  String? money;
  String? tglMulai;
  String? tglAkhir;
  int? updatedBy;
  int? packageId;
  String? pageType;
  String? nonPromo;
  String? imageDetail;
  String? termsConditions;
  String? allProduct;
  String? isShow;

  DataYoutube(
      {this.id,
        this.websiteId,
        this.contentTypeId,
        this.type,
        this.title,
        this.titleEn,
        this.titleBar,
        this.subtitle,
        this.subtitleEn,
        this.slug,
        this.excerpt,
        this.excerptEn,
        this.content,
        this.contentEn,
        this.image,
        this.imageWebview,
        this.order,
        this.seoTitle,
        this.seoKeyword,
        this.seoDescription,
        this.parentId,
        this.isActive,
        this.createdBy,
        this.createdAt,
        this.modified,
        this.viewed,
        this.itemId,
        this.categoryId,
        this.typeId,
        this.url,
        this.code,
        this.quota,
        this.amountType,
        this.amount,
        this.amountMax,
        this.money,
        this.tglMulai,
        this.tglAkhir,
        this.updatedBy,
        this.packageId,
        this.pageType,
        this.nonPromo,
        this.imageDetail,
        this.termsConditions,
        this.allProduct,
        this.isShow});

  DataYoutube.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    websiteId = json['website_id'];
    contentTypeId = json['content_type_id'];
    type = json['type'];
    title = json['title'];
    titleEn = json['title_en'];
    titleBar = json['title_bar'];
    subtitle = json['subtitle'];
    subtitleEn = json['subtitle_en'];
    slug = json['slug'];
    excerpt = json['excerpt'];
    excerptEn = json['excerpt_en'];
    content = json['content'];
    contentEn = json['content_en'];
    image = json['image'];
    imageWebview = json['image_webview'];
    order = json['order'];
    seoTitle = json['seo_title'];
    seoKeyword = json['seo_keyword'];
    seoDescription = json['seo_description'];
    parentId = json['parent_id'];
    isActive = json['is_active'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    modified = json['modified'];
    viewed = json['viewed'];
    itemId = json['item_id'];
    categoryId = json['category_id'];
    typeId = json['type_id'];
    url = json['url'];
    code = json['code'];
    quota = json['quota'];
    amountType = json['amount_type'];
    amount = json['amount'];
    amountMax = json['amount_max'];
    money = json['money'];
    tglMulai = json['tgl_mulai'];
    tglAkhir = json['tgl_akhir'];
    updatedBy = json['updated_by'];
    packageId = json['package_id'];
    pageType = json['page_type'];
    nonPromo = json['non_promo'];
    imageDetail = json['image_detail'];
    termsConditions = json['terms_conditions'];
    allProduct = json['all_product'];
    isShow = json['is_show'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['website_id'] = this.websiteId;
    data['content_type_id'] = this.contentTypeId;
    data['type'] = this.type;
    data['title'] = this.title;
    data['title_en'] = this.titleEn;
    data['title_bar'] = this.titleBar;
    data['subtitle'] = this.subtitle;
    data['subtitle_en'] = this.subtitleEn;
    data['slug'] = this.slug;
    data['excerpt'] = this.excerpt;
    data['excerpt_en'] = this.excerptEn;
    data['content'] = this.content;
    data['content_en'] = this.contentEn;
    data['image'] = this.image;
    data['image_webview'] = this.imageWebview;
    data['order'] = this.order;
    data['seo_title'] = this.seoTitle;
    data['seo_keyword'] = this.seoKeyword;
    data['seo_description'] = this.seoDescription;
    data['parent_id'] = this.parentId;
    data['is_active'] = this.isActive;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['modified'] = this.modified;
    data['viewed'] = this.viewed;
    data['item_id'] = this.itemId;
    data['category_id'] = this.categoryId;
    data['type_id'] = this.typeId;
    data['url'] = this.url;
    data['code'] = this.code;
    data['quota'] = this.quota;
    data['amount_type'] = this.amountType;
    data['amount'] = this.amount;
    data['amount_max'] = this.amountMax;
    data['money'] = this.money;
    data['tgl_mulai'] = this.tglMulai;
    data['tgl_akhir'] = this.tglAkhir;
    data['updated_by'] = this.updatedBy;
    data['package_id'] = this.packageId;
    data['page_type'] = this.pageType;
    data['non_promo'] = this.nonPromo;
    data['image_detail'] = this.imageDetail;
    data['terms_conditions'] = this.termsConditions;
    data['all_product'] = this.allProduct;
    data['is_show'] = this.isShow;
    return data;
  }
}
