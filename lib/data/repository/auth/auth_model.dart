import 'dart:io';

import 'package:dio/dio.dart';
import 'package:motionlife/data/model/login/login.dart';
import 'package:motionlife/config/api/api_exception.dart';

import '../../../config/api/dio_client.dart';
import '../../../config/api/endpoint.dart';
import 'auth_repository.dart';

class AuthModel extends AuthRepository {
  @override
  Future<Login> login({
    required Map<String, dynamic> body,
  }) async {
    var responseData;
    try {
      var formData = FormData.fromMap(body);

      var dio = DioClient.createInstance();
      var response = await dio.post(Endpoint.loginUrl, data: formData);
      var statusCode = response.statusCode;
      if (statusCode == 200) {
        responseData = Login.fromJson(response.data);
      }
    } on DioError catch (e) {
      if (DioErrorType.receiveTimeout == e.type ||
          DioErrorType.connectTimeout == e.type) {
        throw FetchDataException('No Internet connection');
      } else if (DioErrorType.other == e.type) {
        if (e.message.contains('SocketException')) {
          throw FetchDataException('No Internet connection');
        }
      } else if (DioErrorType.response == e.type) {
        var statusCode = e.response!.statusCode;
        if (statusCode == 400) {
          throw BadRequestException(e.response!.data.toString());
        } else if (statusCode == 401 || statusCode == 403) {
          throw UnauthorisedException(e.response!.data.toString());
        } else if (statusCode == 500) {
          throw FetchDataException(
              'Error occured while Communication with Server with StatusCode : ${e.response!.statusCode}');
        }
      }
    }
    return responseData;
  }
}
