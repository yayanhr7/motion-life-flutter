import 'package:flutter/material.dart';
import 'package:motionlife/data/model/login/login.dart';

abstract class AuthRepository {
  Future<Login> login({
    required Map<String, dynamic> body
  });
}
