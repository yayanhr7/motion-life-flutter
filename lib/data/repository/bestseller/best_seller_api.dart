
import 'package:motionlife/data/model/bestseller/best_seller.dart';

abstract class BestSellerApi{
  Future<BestSellerModel> getBestSeller();
}