import 'package:dio/dio.dart';
import 'package:motionlife/data/model/bestseller/best_seller.dart';
import 'package:motionlife/data/repository/bestseller/best_seller_api.dart';

import '../../../config/api/api_exception.dart';
import '../../../config/api/endpoint.dart';

class BestSellerRespository extends BestSellerApi {
  final Dio dio;

  BestSellerRespository(this.dio);

  @override
  Future<BestSellerModel> getBestSeller() async {
    var response;
    try {
      var responseJson = await dio.get(Endpoint.bestSellerUrl);
      var statusCode = responseJson.statusCode;
      print(statusCode);
      if (statusCode == 200) {
        print("tt $responseJson");
        response = BestSellerModel.fromJson(responseJson.data);
      }
    } on DioError catch (e) {
      if (DioErrorType.receiveTimeout == e.type ||
          DioErrorType.connectTimeout == e.type) {
        throw FetchDataException('No Internet connection');
      } else if (DioErrorType.other == e.type) {
        if (e.message.contains('SocketException')) {
          throw FetchDataException('No Internet connection');
        }
      } else if (DioErrorType.response == e.type) {
        var statusCode = e.response!.statusCode;
        if (statusCode == 400) {
          throw BadRequestException(e.response!.data.toString());
        } else if (statusCode == 401 || statusCode == 403) {
          throw UnauthorisedException(e.response!.data.toString());
        } else if (statusCode == 500) {
          throw FetchDataException(
              'Error occured while Communication with Server with StatusCode : ${e.response!.statusCode}');
        }
      }
    }

    return response;
  }
}
