
import 'package:motionlife/data/model/promo/news_model.dart';

abstract class NewsApi{
  Future<List<DataNews>> getNews();
}