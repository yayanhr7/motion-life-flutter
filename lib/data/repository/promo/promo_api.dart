import 'package:motionlife/data/model/promo/promo_model.dart';

abstract class PromoApi{
  Future<PromoModel> getPromo();
}