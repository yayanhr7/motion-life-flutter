
import 'package:dio/dio.dart';
import 'package:motionlife/data/model/promo/promo_model.dart';
import 'package:motionlife/data/repository/promo/promo_api.dart';

import '../../../config/api/api_exception.dart';
import '../../../config/api/endpoint.dart';

class PromoRepository extends PromoApi{

  final Dio dio;
  PromoRepository(this.dio);

  @override
  Future<PromoModel> getPromo() async{
        var responseData;
        try{
          var response = await dio.get(Endpoint.promoUrl);
          var statusCode = response.statusCode;
          if (statusCode == 200) {
            responseData = PromoModel.fromJson(response.data);
          }
        } on DioError catch (e) {
          if (DioErrorType.receiveTimeout == e.type ||
              DioErrorType.connectTimeout == e.type) {
            throw FetchDataException('No Internet connection');
          } else if (DioErrorType.other == e.type) {
            if (e.message.contains('SocketException')) {
              throw FetchDataException('No Internet connection');
            }
          } else if (DioErrorType.response == e.type) {
            var statusCode = e.response!.statusCode;
            if (statusCode == 400) {
              throw BadRequestException(e.response!.data.toString());
            } else if (statusCode == 401 || statusCode == 403) {
              throw UnauthorisedException(e.response!.data.toString());
            } else if (statusCode == 500) {
              throw FetchDataException(
                  'Error occured while Communication with Server with StatusCode : ${e.response!.statusCode}');
            }
          }
        }

        return responseData;
  }

}