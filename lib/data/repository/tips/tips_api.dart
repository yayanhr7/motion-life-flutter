import 'package:motionlife/data/model/tips/tips_model.dart';

abstract class TipsApi{
  Future<TipsModel> getTips();
}