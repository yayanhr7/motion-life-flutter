import 'package:motionlife/data/model/youtube/youtube_model.dart';

abstract class YoutubeApi{
  Future<YoutubeModel> getYoutube();
}