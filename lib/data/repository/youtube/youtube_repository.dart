import 'package:dio/dio.dart';
import 'package:motionlife/data/model/youtube/youtube_model.dart';
import 'package:motionlife/data/repository/youtube/youtube_api.dart';

import '../../../config/api/api_exception.dart';
import '../../../config/api/endpoint.dart';

class YoutubeRepository extends YoutubeApi{
  final Dio dio;
  YoutubeRepository(this.dio);

  @override
  Future<YoutubeModel> getYoutube() async{
    var responseData;
    try{
      var response = await dio.get(Endpoint.youtubeUrl);
      var statusCode = response.statusCode;
      if (statusCode == 200) {
        responseData = YoutubeModel.fromJson(response.data);
      }
    } on DioError catch (e) {
      if (DioErrorType.receiveTimeout == e.type ||
          DioErrorType.connectTimeout == e.type) {
        throw FetchDataException('No Internet connection');
      } else if (DioErrorType.other == e.type) {
        if (e.message.contains('SocketException')) {
          throw FetchDataException('No Internet connection');
        }
      } else if (DioErrorType.response == e.type) {
        var statusCode = e.response!.statusCode;
        if (statusCode == 400) {
          throw BadRequestException(e.response!.data.toString());
        } else if (statusCode == 401 || statusCode == 403) {
          throw UnauthorisedException(e.response!.data.toString());
        } else if (statusCode == 500) {
          throw FetchDataException(
              'Error occured while Communication with Server with StatusCode : ${e.response!.statusCode}');
        }
      }
    }

    return responseData;
  }

}