import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:motionlife/utils/pref.dart';

import 'config/routes/router.dart';

var isFirstTime = false;
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  isFirstTime = await Pref.isFirstTime();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(378, 812),
      builder: () => MaterialApp(
        title: 'Motio Life',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(primarySwatch: Colors.blue, fontFamily: 'DMSans'),
        onGenerateRoute: AppRouter.generateRoute,
        initialRoute: isFirstTime? AppRouter.onboard: AppRouter.dashboard,
      ),
    );
  }
}
