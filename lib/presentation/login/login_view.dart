import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:motionlife/business_logic/bloc/auth/auth_bloc.dart';
import 'package:motionlife/data/model/request_state.dart';
import 'package:motionlife/data/model/login/login.dart';
import 'package:motionlife/config/routes/router.dart';
import 'package:motionlife/presentation/nointernet/nointernet.dart';
import 'package:motionlife/utils/color.dart';
import 'package:motionlife/presentation/widget/loading/loading.dart';
import 'package:motionlife/utils/socialicon.dart';
import 'package:motionlife/utils/userpref.dart';

class LoginView extends StatefulWidget {
  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final _loginBloc = AuthBloc();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  bool isError = false;
  bool isEmailEmpty = false;
  bool isPasswordEmpty = false;
  bool showPassword = true;
  String message = "";

  late Login loginResponse;

  final Map<String, dynamic> _loginData = {
    'EMAIL': '',
    'PASSWORD': '',
    'SOURCE': 'EMAIL',
    'ACTION': 'SIGN_IN'
  };

  // GoogleSignIn _googleSignIn = GoogleSignIn(
  //   scopes: [
  //     'email',
  //     'https://www.googleapis.com/auth/contacts.readonly',
  //   ],
  // );
  //
  // late GoogleSignInAccount? _currentUser;
  //
  // Future<void> _signInGoogle() async {
  //   try {
  //     _currentUser = await _googleSignIn.signIn();
  //     print("asa");
  //     _currentUser!.authentication.then((value) => {
  //           print(value)
  //           // _signInWithGoogle(value.idToken)
  //         });
  //   } catch (error) {
  //     print(error);
  //   }
  // }
  //
  // Future<void> _signInApple() async {
  //   final credential = await SignInWithApple.getAppleIDCredential(
  //     scopes: [
  //       AppleIDAuthorizationScopes.email,
  //       AppleIDAuthorizationScopes.fullName,
  //     ],
  //   );
  //
  //   print(credential);
  // }

  _login() {
    var email = _emailController.text;
    var password = _passwordController.text;
    setState(() {
      if (email.isEmpty) {
        isEmailEmpty = true;
      } else {
        isEmailEmpty = false;
      }
    });

    setState(() {
      if (password.isEmpty) {
        isPasswordEmpty = true;
      } else {
        isPasswordEmpty = false;
      }
    });

    if (!isPasswordEmpty && !isEmailEmpty) {
      setState(() {
        _loginData['EMAIL'] = email;
        _loginData['PASSWORD'] = password;
      });
      _loginBloc.login(_loginData);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        elevation: 1,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_rounded,
            color: AppColor.bluish,
            size: 16,
          ),
          onPressed: null,
        ),
        backgroundColor: Colors.white,
      ),
      body: StreamBuilder<RequestState<Login>>(
          stream: _loginBloc.loginStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              switch (snapshot.data!.status) {
                case Status.LOADING:
                  return Loading();
                case Status.COMPLETED:
                  loginResponse = snapshot.data!.data;
                  var statusCode = loginResponse.statusCode;
                  if (statusCode == "00") {
                    UserPref.saveUserLogin(loginResponse.data.first);
                    isError = false;
                    WidgetsBinding.instance!.addPostFrameCallback(
                      (_) => Navigator.of(context).pushReplacementNamed(
                        AppRouter.dashboard,
                      ),
                    );
                  } else {
                    isError = true;
                    message = loginResponse.desc;
                  }
                  break;
                case Status.ERROR:
                  message = snapshot.data!.message;
                  if (message.contains("No Internet")) {
                    return NoInternet(_login);
                  } else {
                    isError = true;
                  }
                  break;
              }
            }
            // if (isInternetError) {
            //   return NoInternet(isInternetError, _login);
            // } else {
            return Container(
              padding: EdgeInsets.symmetric(
                horizontal: 20.w,
              ),
              color: AppColor.grey98,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextField(
                    controller: _emailController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.r),
                      ),
                      hintText: "Email",
                      labelText: "Email",
                      errorText: isEmailEmpty ? "Email harus diisi" : null,
                      contentPadding: const EdgeInsets.symmetric(
                        horizontal: 12,
                        vertical: 10,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 14.h,
                  ),
                  TextField(
                    controller: _passwordController,
                    obscureText: showPassword,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.r),
                      ),
                      suffixIcon: IconButton(
                        icon: showPassword
                            ? Icon(
                                Icons.remove_red_eye,
                                color: AppColor.grey,
                              )
                            : Icon(
                                Icons.remove_red_eye_outlined,
                                color: AppColor.grey,
                              ),
                        onPressed: () =>
                            setState(() => showPassword = !showPassword),
                      ),
                      hintText: "Password",
                      labelText: "Password",
                      errorText:
                          isPasswordEmpty ? "Password harus diisi" : null,
                      contentPadding: const EdgeInsets.symmetric(
                        horizontal: 12,
                        vertical: 10,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 8.h,
                  ),
                  Visibility(
                    visible: isError,
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Column(
                        children: [
                          Text(
                            message,
                            style: TextStyle(
                              color: AppColor.crimson,
                            ),
                          ),
                          SizedBox(
                            height: 8.h,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: Text(
                      "Lupa kata sandi?",
                      style: TextStyle(
                        fontSize: 12.sp,
                        color: AppColor.grey58,
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(
                      top: 20,
                      bottom: 12,
                    ),
                    child: SizedBox(
                      width: double.infinity,
                      height: 46.h,
                      child: ElevatedButton(
                        style: TextButton.styleFrom(
                          backgroundColor: AppColor.primary,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(16),
                          ),
                        ),
                        onPressed: _login,
                        child: const Text(
                          "Masuk",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Belum punya akun?",
                        style: TextStyle(
                          color: AppColor.grey58,
                        ),
                      ),
                      SizedBox(
                        width: 6.w,
                      ),
                      Text(
                        "Daftar",
                        style: TextStyle(
                          color: AppColor.bluish,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      SizedBox(
                        height: 20.h,
                      ),
                    ],
                  ),
                  Container(
                    padding: const EdgeInsets.only(
                      top: 20,
                      left: 75,
                      right: 75,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Expanded(
                          child: Divider(
                            color: AppColor.grey,
                          ),
                        ),
                        SizedBox(
                          width: 18.w,
                        ),
                        Text(
                          "Atau",
                          style: TextStyle(color: AppColor.grey58),
                        ),
                        SizedBox(
                          width: 18.w,
                        ),
                        Expanded(
                          child: Divider(
                            color: AppColor.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(
                      vertical: 26.h,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          onPressed: (){},
                          child: Icon(SocialIcon.facebook, color: Colors.blue),
                          style: ElevatedButton.styleFrom(
                              shape: CircleBorder(),
                              padding: EdgeInsets.all(16),
                              primary: Colors.white),
                        ),
                        SizedBox(
                          width: 26.w,
                        ),
                        ElevatedButton(
                          onPressed: () {},
                          child: Icon(SocialIcon.facebook, color: Colors.blue),
                          style: ElevatedButton.styleFrom(
                              shape: CircleBorder(),
                              padding: EdgeInsets.all(16),
                              primary: Colors.white),
                        ),
                        SizedBox(
                          width: 26.w,
                        ),
                        if (Platform.isIOS)
                          ElevatedButton(
                            onPressed: (){},
                            child:
                                Icon(SocialIcon.facebook, color: Colors.blue),
                            style: ElevatedButton.styleFrom(
                                shape: CircleBorder(),
                                padding: EdgeInsets.all(16),
                                primary: Colors.white),
                          )
                      ],
                    ),
                  ),
                ],
              ),
            );
            // }
          }),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _loginBloc.dispose();
  }
}
