import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:motionlife/utils/color.dart';
class NoInternet extends StatefulWidget {

  Function _login;

  NoInternet(this._login);

  @override
  _NoInternetState createState() => _NoInternetState();
}

class _NoInternetState extends State<NoInternet> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            "assets/images/nointernet.png",
            width: 132.w,
            height: 156.h,
          ),
          SizedBox(
            height: 22.h,
          ),
          Text(
            "Tidak ada koneksi internet",
            style: TextStyle(
              color: AppColor.greyWarm,
              fontWeight: FontWeight.w700,
              fontSize: 16.sp,
            ),
          ),
          SizedBox(
            height: 14.h,
          ),
          Text(
            "Silahkan coba lagi",
            style: TextStyle(
              color: AppColor.grey58,
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 20.w,
              vertical: 36.h,
            ),
            child: SizedBox(
              width: double.infinity,
              height: 56.h,
              child: ElevatedButton(
                style: TextButton.styleFrom(
                  backgroundColor: AppColor.primary,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16),
                  ),
                ),
                onPressed: (){
                  widget._login();
                },
                child: const Text(
                  "Coba Lagi",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
