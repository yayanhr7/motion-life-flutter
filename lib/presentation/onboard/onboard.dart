import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:motionlife/data/data_provider/onboard/onboardmodel.dart';
import 'package:motionlife/config/routes/router.dart';
import 'package:motionlife/utils/color.dart';
import 'package:motionlife/utils/default_dot.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OnBoard extends StatefulWidget {

  @override
  _OnBoardState createState() => _OnBoardState();
}

class _OnBoardState extends State<OnBoard> {
  List<OnboardModel> list = <OnboardModel>[];

  int currentIndex = 0;
  PageController _controller = PageController(initialPage: 0);

  Future<bool> isFirstTime() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    var isFirstTime = pref.getBool('first_time');
    if (isFirstTime != null && !isFirstTime) {
      pref.setBool('first_time', false);
      return false;
    } else {
      pref.setBool('first_time', false);
      return true;
    }
  }

  @override
  void initState() {
    super.initState();

    list = getOnBoardData();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          children: [
            SizedBox(
              height: 94.h,
            ),
            Expanded(
              child: PageView.builder(
                controller: _controller,
                onPageChanged: (value) {
                  setState(() {
                    currentIndex = value;
                  });
                },
                itemCount: list.length,
                itemBuilder: (BuildContext context, int index) {
                  return OnboardView(
                    list[index].image,
                  );
                },
              ),
            ),
            SizedBox(
              height: 52.h,
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: List.generate(
                  list.length,
                  (index) => DefaultDot(currentIndex, index, AppColor.pictonBlue),
                ),
              ),
            ),
            SizedBox(
              height: 33.h,
            ),
            Align(
              alignment: FractionalOffset.bottomCenter,
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 24.w,
                ),
                // const EdgeInsets.only(left: 24, top: 33, right: 24, bottom: 48),
                child: SizedBox(
                  width: double.infinity,
                  height: 44.h,
                  child: ElevatedButton(
                    style: TextButton.styleFrom(
                      backgroundColor: AppColor.primary,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.r),
                      ),
                    ),
                    onPressed: () {
                      if (currentIndex == list.length - 1) {
                        Navigator.of(context).pushReplacementNamed(
                          AppRouter.loginRoute,
                        );
                      } else {
                        _controller.nextPage(
                            duration: Duration(milliseconds: 100),
                            curve: Curves.bounceIn);
                      }
                    },
                    child: Text(
                      currentIndex == list.length - 1 ? "Mulai" : "Lanjutkan",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 14.sp,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 48.h,
            ),
          ],
        ),
      ),
    );
  }
}

class OnboardView extends StatelessWidget {
  String image;

  OnboardView(this.image, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Image.asset(
          image,
          width: 302.w,
          height: 437.h,
        ),
      ],
    );
  }
}
