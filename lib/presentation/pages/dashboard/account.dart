import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:motionlife/data/model/login/login.dart';
import 'package:motionlife/config/routes/router.dart';
import 'package:motionlife/presentation/widget/card/card_default.dart';
import 'package:motionlife/presentation/widget/card/card_default_rounded.dart';
import 'package:motionlife/utils/color.dart';
import 'package:motionlife/utils/userpref.dart';


class Account extends StatelessWidget {
  DataLogin? userData;

  Account(this.userData);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(
          "Akun",
          style: TextStyle(
            color: AppColor.nileBlue,
          ),
        ),
        elevation: 1,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_rounded,
            color: AppColor.linkWater,
            size: 16,
          ),
          onPressed: null,
        ),
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        child: Container(
          color: AppColor.grey90,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 16.w,
                  vertical: 20.h,
                ),
                color: Colors.white,
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: 10.w,
                      ),
                      child: CardDefault(
                        userData!.fullname,
                        userData!.email,
                        "assets/images/ic_avatar.svg",
                        "assets/images/ic_edit.svg",
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                        top: 19.h,
                        bottom: 10.h,
                      ),
                      child: Image.asset("assets/images/ic_aktivasi.png"),
                    ),
                    CardDefault(
                      "Undang Teman",
                      "Undang teman untuk gunakan MotionInsiure, kamu bisa dapakan hadiah menarik",
                      "assets/images/ic_invite_people.svg",
                      "assets/images/ic_arrow_forward.svg",
                    ),
                    CardDefault(
                      "Kupon",
                      "Pakai kuponmu, dapatkan potongan harga",
                      "assets/images/ic_coupon.svg",
                      "assets/images/ic_arrow_forward.svg",
                    ),
                    CardDefault(
                      "Input Nomor Polis",
                      "Masukkan nomor polis terdaftar disini",
                      "assets/images/ic_polis_blue.svg",
                      "assets/images/ic_arrow_forward.svg",
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 16.w,
                  vertical: 10.h,
                ),
                child: Column(
                  children: [
                    CardDefaultRounded(
                      "Ubah Kata Sandi",
                      "assets/images/ic_change_password.svg",
                      "assets/images/ic_arrow_forward.svg",
                    ),
                    CardDefaultRounded(
                      "Syarat & ketentuan",
                      "assets/images/ic_agreement.svg",
                      "assets/images/ic_arrow_forward.svg",
                    ),
                    CardDefaultRounded(
                      "FAQ",
                      "assets/images/ic_faq.svg",
                      "assets/images/ic_arrow_forward.svg",
                    ),
                    CardDefaultRounded(
                      "Pusat Bantuan",
                      "assets/images/ic_help_support.svg",
                      "assets/images/ic_arrow_forward.svg",
                    ),
                    CardDefaultRounded(
                      "Tentang Motion Insure",
                      "assets/images/ic_info.svg",
                      "assets/images/ic_arrow_forward.svg",
                    ),
                    Container(
                      padding: const EdgeInsets.only(
                        top: 20,
                        bottom: 12,
                      ),
                      child: SizedBox(
                        width: double.infinity,
                        height: 46.h,
                        child: ElevatedButton(
                          style: TextButton.styleFrom(
                            backgroundColor: AppColor.grey90,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(16),
                              side: BorderSide(color: AppColor.crimson),
                            ),
                          ),
                          onPressed: () {
                            UserPref.removeUserLogin();
                            Navigator.of(context)
                                .pushReplacementNamed(AppRouter.loginRoute);
                          },
                          child: Text(
                            "Keluar",
                            style: TextStyle(
                              color: AppColor.crimson,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
