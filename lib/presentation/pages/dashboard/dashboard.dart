import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:motionlife/data/model/login/login.dart';
import 'package:motionlife/presentation/pages/dashboard/account.dart';
import 'package:motionlife/presentation/pages/dashboard/home/home.dart';
import 'package:motionlife/presentation/pages/dashboard/notif.dart';
import 'package:motionlife/presentation/pages/dashboard/polis.dart';
import 'package:motionlife/utils/color.dart';
import 'package:motionlife/utils/userpref.dart';

import '../../login/login_view.dart';


class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  var _selectedIndex = 0;

  DataLogin? userdata;

  Future<DataLogin> getUserData() async{
    var data = await UserPref.getUserLogin();
    setState(() {
        userdata = data;
    });

    return userdata!;
  }

  @override
  void initState() {
    super.initState();
    getUserData();
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return userdata == null? LoginView() : Scaffold(
      resizeToAvoidBottomInset: true,
      body: IndexedStack(
        index: _selectedIndex,
        children: [
          Home(),
          userdata == null? LoginView() : Polis(),
          userdata == null? LoginView() : Notif(),
          userdata == null? LoginView() :  Account(userdata),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        showSelectedLabels: true,
        showUnselectedLabels: true,
        unselectedItemColor: AppColor.slateGrey,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              "assets/images/ic_home_grey.svg",
            ),
            activeIcon: SvgPicture.asset(
              "assets/images/ic_home_blue.svg",
            ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              "assets/images/ic_polis_grey.svg",
            ),
            activeIcon: SvgPicture.asset(
              "assets/images/ic_polis_blue.svg",
            ),
            label: 'Polis',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              "assets/images/ic_notif_grey.svg",
            ),
            activeIcon: SvgPicture.asset(
              "assets/images/ic_notif_blue.svg",
            ),
            label: 'Notifikasi',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              "assets/images/ic_account_grey.svg",
            ),
            activeIcon: SvgPicture.asset(
              "assets/images/ic_account_blue.svg",
            ),
            label: 'Akun',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: AppColor.pictonBlue,
        onTap: _onItemTapped,
      ),
    );
  }
}
