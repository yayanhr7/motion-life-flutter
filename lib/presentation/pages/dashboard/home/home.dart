import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:motionlife/business_logic/bloc/bestseller/best_seller_bloc.dart';
import 'package:motionlife/business_logic/bloc/news/news_bloc.dart';
import 'package:motionlife/business_logic/bloc/promo/pomo_bloc.dart';
import 'package:motionlife/business_logic/bloc/tips/tips_bloc.dart';
import 'package:motionlife/business_logic/bloc/youtube/youtube_bloc.dart';
import 'package:motionlife/data/model/bestseller/best_seller.dart';
import 'package:motionlife/data/model/promo/news_model.dart';
import 'package:motionlife/data/model/promo/promo_model.dart';
import 'package:motionlife/data/model/request_state.dart';
import 'package:motionlife/data/model/tips/tips_model.dart';
import 'package:motionlife/data/model/youtube/youtube_model.dart';
import 'package:motionlife/presentation/widget/builder/row_builder.dart';
import 'package:motionlife/presentation/widget/home/banner_best_seller.dart';
import 'package:motionlife/presentation/widget/home/banner_image_promo.dart';
import 'package:motionlife/presentation/widget/home/banner_news.dart';
import 'package:motionlife/presentation/widget/home/banner_tips.dart';
import 'package:motionlife/presentation/widget/home/banner_youtube.dart';
import 'package:motionlife/presentation/widget/home/menu.dart';
import 'package:motionlife/presentation/widget/loading/skeleton_banner.dart';
import 'package:motionlife/presentation/widget/loading/skeleton_full_banner.dart';
import 'package:motionlife/presentation/widget/loading/skeleton_half_banner.dart';
import 'package:motionlife/presentation/widget/loading/skeleton_half_height.dart';
import 'package:motionlife/presentation/widget/loading/skeleton_loading.dart';
import 'package:motionlife/presentation/widget/nointernet/no_internet.dart';
import 'package:motionlife/utils/color.dart';
import 'package:skeletons/skeletons.dart';

class Home extends StatefulWidget {
  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var listMenu = {
    "Asuransi\nKecelakaan": "assets/images/ic_kecelakaan.png",
    "Asuransi\nKesehatan": "assets/images/ic_kesehatan.png",
    "Asuransi\nJiwa": "assets/images/ic_jiwa.png",
    "Employee\nBenefit": "assets/images/ic_employe_benefit.png",
    "Konsultasi\nDokter": "assets/images/ic_konsultasi.png",
    "Periksa\nKesehatan": "assets/images/ic_periksa.png"
  };

  int currentIndex = 0;

  final _promoBloc = PromoBloc();
  final _newsBloc = NewsBloc();
  final _bestSellerBloc = BestSellerBloc();
  final _tipsBloc = TipsBloc();
  final _youtubeBloc = YoutubeBloc();

  Future<PromoModel?> getPromo() async {
    await _promoBloc.getPromo();
  }

  Future<DataNews?> getNews() async {
    await _newsBloc.getNews();
  }

  Future<BestSellerModel?> getBestSeller() async {
    await _bestSellerBloc.getBestSeller();
  }

  Future<YoutubeModel?> getYoutube() async {
    await _youtubeBloc.getYoutube();
  }

  Future<TipsModel?> getTips() async {
    await _tipsBloc.getTips();
  }

  @override
  void initState() {
    super.initState();
    getPromo();
    getBestSeller();
    getTips();
    getYoutube();
    // getNews();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          StreamBuilder<RequestState<PromoModel>>(
              stream: _promoBloc.promoStream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  switch (snapshot.data!.status) {
                    case Status.LOADING:
                      return SkeletonFullBanner();

                    case Status.COMPLETED:
                      final promoData = snapshot.data!.data;
                      return SizedBox(
                        height: 243.h,
                        child: PageView.builder(
                          onPageChanged: (value) {
                            setState(() {
                              currentIndex = value;
                            });
                          },
                          itemCount: promoData.data!.length,
                          itemBuilder: (BuildContext context, int index) =>
                              BannerImagePromo(
                            promoData.data!,
                            currentIndex,
                          ),
                        ),
                      );

                    case Status.ERROR:
                      final error = snapshot.data!.message;
                      if (error.toLowerCase().contains("no internet")) {
                        return GestureDetector(
                          onTap: getPromo,
                          child: NoInternet(),
                        );
                      } else {
                        return Center(child: Text(error));
                      }
                      break;
                  }
                }

                return const Text("asd");
              }),
          GridView(
            physics: const NeverScrollableScrollPhysics(),
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3),
            shrinkWrap: true,
            children: listMenu.entries
                .map(
                  (e) => Menu(
                    e.key,
                    e.value,
                  ),
                )
                .toList(),
          ),
          Container(
            color: AppColor.grey90,
            width: double.infinity,
            height: 5.h,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.symmetric(
                  horizontal: 24.w,
                  vertical: 20.h,
                ),
                alignment: FractionalOffset.topLeft,
                child: Text(
                  "Terbaru dari MotionLife",
                  style: TextStyle(
                    color: AppColor.nileBlue,
                    fontSize: 16.sp,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: 7.w,
                  ),
                  child: Row(
                    children: [
                      BannerNews(),
                      BannerNews(),
                      BannerNews(),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Container(
            color: AppColor.grey90,
            width: double.infinity,
            height: 5.h,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.symmetric(
                  horizontal: 24.w,
                  vertical: 20.h,
                ),
                alignment: FractionalOffset.topLeft,
                child: Text(
                  "Asuransi Paling Diminati, nih!",
                  style: TextStyle(
                    color: AppColor.nileBlue,
                    fontSize: 16.sp,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: 7.w,
                  ),
                  child: StreamBuilder<RequestState<BestSellerModel>>(
                      stream: _bestSellerBloc.bestSellerStream,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          switch (snapshot.data!.status) {
                            case Status.LOADING:
                              return RowBuilder(
                                  itemCount: 3,
                                  itemBuilder: (context, index) =>
                                      SkeletonHalfBanner());

                            case Status.COMPLETED:
                              var data = snapshot.data!.data.data;
                              final activeData = data
                                  ?.where((element) => element.isActive == 1);
                              return Row(
                                  children: activeData!
                                      .map((e) => BannerBestSeller(e))
                                      .toList());

                            case Status.ERROR:
                              final error = snapshot.data!.message;
                              if (error.toLowerCase().contains("no internet")) {
                                return Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: GestureDetector(
                                      onTap: getBestSeller,
                                      child: NoInternet()),
                                );
                              } else {
                                return Center(child: Text(error));
                              }
                          }
                        }
                        return Container(
                          margin: EdgeInsets.only(
                            left: 10.w,
                            bottom: 20.w,
                          ),
                          child: SkeletonAvatarLoading(
                            157.w,
                            157.h,
                          ),
                        );
                      }),
                ),
              ),
              Container(
                color: AppColor.grey90,
                width: double.infinity,
                height: 5.h,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: 24.w,
                      vertical: 20.h,
                    ),
                    alignment: FractionalOffset.topLeft,
                    child: Text(
                      "Rumah Sakit Terdekat",
                      style: TextStyle(
                        color: AppColor.nileBlue,
                        fontSize: 16.sp,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: 7.w,
                    ),
                    child: Card(
                      clipBehavior: Clip.antiAliasWithSaveLayer,
                      margin: EdgeInsets.only(
                        left: 10.w,
                        bottom: 20.w,
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(
                          10.r,
                        ),
                      ),
                      elevation: 4,
                      child: Image.asset(
                        "assets/images/banner_hospital.png",
                        width: double.infinity,
                        height: 150.h,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                color: AppColor.grey90,
                width: double.infinity,
                height: 5.h,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: 24.w,
                      vertical: 20.h,
                    ),
                    alignment: FractionalOffset.topLeft,
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            "Info dan Tips",
                            style: TextStyle(
                              color: AppColor.nileBlue,
                              fontSize: 16.sp,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Text(
                          "Lihat Semua",
                          style: TextStyle(
                            color: AppColor.primary,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Container(
                      margin: EdgeInsets.symmetric(
                        horizontal: 7.w,
                      ),
                      child: StreamBuilder<RequestState<TipsModel>>(
                          stream: _tipsBloc.tipsStream,
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              switch (snapshot.data!.status) {
                                case Status.LOADING:
                                  return RowBuilder(
                                    itemCount: 3,
                                    itemBuilder: (context, index) =>
                                        SkeletonBanner(),
                                  );
                                  break;
                                case Status.COMPLETED:
                                  var data = snapshot.data!.data;
                                  final activeData = data.data!.where(
                                      (element) => element.isActive == 1);
                                  return Row(
                                      children: activeData
                                          .map((e) => BannerTips(e))
                                          .toList()
                                      // BannerNews(),
                                      );
                                  break;
                                case Status.ERROR:
                                  final error = snapshot.data!.message;
                                  if (error
                                      .toLowerCase()
                                      .contains("no internet")) {
                                    return Container(
                                      width: MediaQuery.of(context).size.width,
                                      child: GestureDetector(
                                          onTap: getTips,
                                          child: NoInternet()),
                                    );
                                  } else {
                                    return Center(child: Text(error));
                                  }
                                  break;
                              }
                            }
                            return const SkeletonLine(
                              style: SkeletonLineStyle(
                                height: double.infinity,
                              ),
                            );
                          }),
                    ),
                  ),
                ],
              ),
              Container(
                color: AppColor.grey90,
                width: double.infinity,
                height: 5.h,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: 24.w,
                      vertical: 20.h,
                    ),
                    alignment: FractionalOffset.centerLeft,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Tonton Video",
                          style: TextStyle(
                            color: AppColor.nileBlue,
                            fontSize: 16.sp,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            top: 7.h,
                          ),
                          child: Text(
                            "Selalu ada video terbaru info seputar\nMotion Insure",
                            style: TextStyle(
                              color: AppColor.slateGrey,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Container(
                      margin: EdgeInsets.symmetric(
                        horizontal: 7.w,
                      ),
                      child: StreamBuilder<RequestState<YoutubeModel>>(
                          stream: _youtubeBloc.youtubeStream,
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              switch (snapshot.data!.status) {
                                case Status.LOADING:
                                  return RowBuilder(
                                    itemCount: 3,
                                    itemBuilder: (context, index) =>
                                        SkeletonHalfHeight(),
                                  );
                                  break;
                                case Status.COMPLETED:
                                  var data = snapshot.data!.data;
                                  final activeData = data.data!.where(
                                      (element) => element.isActive == 1);
                                  return Row(
                                      children: activeData
                                          .map((e) => BannerYoutube(e))
                                          .toList());
                                  break;
                                case Status.ERROR:
                                  final error = snapshot.data!.message;
                                  if (error
                                      .toLowerCase()
                                      .contains("no internet")) {
                                    return Container(
                                      width: MediaQuery.of(context).size.width,
                                      child: GestureDetector(
                                          onTap: getYoutube,
                                          child: NoInternet()),
                                    );
                                  } else {
                                    return Center(child: Text(error));
                                  }
                                  break;
                              }
                            }
                            return Text("a");
                          }),
                    ),
                  ),
                  Container(
                    color: AppColor.grey90,
                    width: double.infinity,
                    height: 5.h,
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
