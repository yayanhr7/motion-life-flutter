import 'package:flutter/material.dart';
import 'package:motionlife/presentation/widget/card/card_notif.dart';
import 'package:motionlife/utils/color.dart';

class Notif extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(
          "Notifikasi",
          style: TextStyle(
            color: AppColor.nileBlue,
          ),
        ),
        elevation: 1,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_rounded,
            color: AppColor.linkWater,
            size: 16,
          ),
          onPressed: null,
        ),
        backgroundColor: Colors.white,
      ),
      body: Column(
        children: [
          CardNotif(),
          CardNotif(),
          CardNotif(),
          CardNotif(),
        ],
      )
    );
  }
}
