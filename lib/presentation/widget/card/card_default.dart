import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:motionlife/utils/color.dart';


class CardDefault extends StatelessWidget {
  final String _title;
  final String _subTitle;
  final String _icLeft;
  final String _icRight;

  CardDefault(this._title, this._subTitle, this._icLeft, this._icRight);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        top: 14.h,
        bottom: 14.h,
        left: 12.w,
        right: 18.w,
      ),
      child: Row(
        children: [
          SvgPicture.asset(_icLeft),
          SizedBox(
            width: 10.w,
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  _title,
                  style: TextStyle(
                    color: AppColor.nileBlue,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Text(
                  _subTitle,
                  style: TextStyle(
                    color: AppColor.slateGrey,
                    fontSize: 12.sp,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            width: 16.w,
          ),
          SvgPicture.asset(_icRight),
        ],
      ),
    );
  }
}
