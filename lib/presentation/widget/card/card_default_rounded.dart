import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:motionlife/utils/color.dart';

class CardDefaultRounded extends StatelessWidget {
  final String _title;
  final String _icLeft;
  final String _icRight;

  CardDefaultRounded(this._title, this._icLeft, this._icRight);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(
          15.r,
        ),
      ),
      child: Padding(
        padding: EdgeInsets.only(
          top: 20.h,
          bottom: 20.h,
          left: 12.w,
          right: 18.w,
        ),
        child: Row(
          children: [
            SvgPicture.asset(_icLeft),
            SizedBox(
              width: 10.w,
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    _title,
                    style: TextStyle(
                      color: AppColor.nileBlue,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 16.w,
            ),
            SvgPicture.asset(_icRight),
          ],
        ),
      ),
    );
  }
}
