import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:motionlife/utils/color.dart';


class CardNotif extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        bottom: 14.h,
        top: 14.h,
        left: 12.w,
        right: 18.w,
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset("assets/images/ic_notif.png"),
              SizedBox(
                width: 10.w,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Notification title",
                    style: TextStyle(
                      color: AppColor.nileBlue,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Text(
                    "Ambil gratisan kamu sekarang juga",
                    style: TextStyle(
                      color: AppColor.slateGrey,
                      fontSize: 12.sp,
                    ),
                  ),
                  Text(
                    "14-11-2021 15:18",
                    style: TextStyle(
                      color: AppColor.slateGrey,
                      fontSize: 12.sp,
                    ),
                  ),
                ],
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(
              top: 16.h,
            ),
            color: AppColor.grey90,
            width: double.infinity,
            height: 1.h,
          ),
        ],
      ),
    );
  }
}
