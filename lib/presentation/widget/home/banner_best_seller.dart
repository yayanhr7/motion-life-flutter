import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:motionlife/data/model/bestseller/best_seller.dart';

import '../../../config/api/endpoint.dart';


class BannerBestSeller extends StatelessWidget {

  DataBestSeller bestSeller;

  BannerBestSeller(this.bestSeller);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 157.w,
      height: 157.h,
      child: Card(
        clipBehavior: Clip.antiAliasWithSaveLayer,
        margin: EdgeInsets.only(
          left: 10.w,
          bottom: 20.w,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
            10.r,
          ),
        ),
        elevation: 4,
        child: Image.network(
          Endpoint.baseUrl + bestSeller.image!,
          height: double.infinity,
          width: double.infinity,
          fit: BoxFit.fill,
        ),
      ),
    );
  }
}
