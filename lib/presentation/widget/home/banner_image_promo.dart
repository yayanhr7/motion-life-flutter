import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:motionlife/data/model/promo/promo_model.dart';

import '../../../config/api/endpoint.dart';
import '../../../utils/color.dart';
import '../../../utils/default_dot.dart';

class BannerImagePromo extends StatelessWidget {
  final List<DataPromo> promo;
  final int _currentIndex;

  BannerImagePromo(this.promo, this._currentIndex);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Image.network(
          Endpoint.baseUrl + promo[_currentIndex].image!,
          fit: BoxFit.fill,
          width: double.infinity,
          height: double.infinity,
        ),
        Container(
          margin: EdgeInsets.only(
            left: 26.w,
            bottom: 11.h,
          ),
          child: Align(
              alignment: FractionalOffset.bottomLeft,
              child: Row(
                children: List.generate(
                  promo.length,
                  (index) => DefaultDot(
                    _currentIndex,
                    index,
                    AppColor.slateGrey,
                  ),
                ),
              )),
        ),
        Align(
          alignment: FractionalOffset.bottomRight,
          child: Container(
            margin: EdgeInsets.symmetric(
              horizontal: 20.w,
              vertical: 4.h,
            ),
            decoration: BoxDecoration(
              color: AppColor.primary,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(
                5.r,
              ),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 8.w,
                vertical: 8.h,
              ),
              child: const Text(
                "Lihat Semua Promo",
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
