import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../utils/color.dart';

class BannerNews extends StatelessWidget {
  const BannerNews({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width * 0.93,
      child: Card(
        clipBehavior: Clip.antiAliasWithSaveLayer,
        margin: EdgeInsets.only(
          left: 10.w,
          bottom: 20.w,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
            10.r,
          ),
        ),
        elevation: 4,
        child: Column(
          children: [
            Image.asset(
              "assets/images/sample_banner_promo.png",
              height: 140.h,
              width: double.infinity,
              fit: BoxFit.fill,
            ),
            Container(
              margin: EdgeInsets.only(
                top: 8.h,
                left: 10.w,
                right: 10.w,
                bottom: 4.h,
              ),
              alignment: FractionalOffset.topLeft,
              child: Text(
                "Lindungi keluarga dengan asuransi",
                style: TextStyle(
                  color: AppColor.nileBlue,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                left: 10.w,
                right: 10.w,
                bottom: 17.h,
              ),
              alignment: FractionalOffset.topLeft,
              child: Text(
                "Sekarang perlindungan keluarga mudah dengan Hario App",
                style: TextStyle(
                  color: AppColor.slateGrey,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
