import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:motionlife/data/model/tips/tips_model.dart';

import '../../../config/api/endpoint.dart';
import '../../../utils/color.dart';


class BannerTips extends StatelessWidget {
  final DataTips dataTips;

  BannerTips(this.dataTips);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width * 0.93,
      height: 243.h,
      child: Card(
        clipBehavior: Clip.antiAliasWithSaveLayer,
        margin: EdgeInsets.only(
          left: 10.w,
          bottom: 20.w,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
            10.r,
          ),
        ),
        elevation: 4,
        child: Column(
          children: [
            Image.network(
              Endpoint.baseUrl + dataTips.image!,
              height: 140.h,
              width: double.infinity,
              fit: BoxFit.fill,
            ),
            Container(
              margin: EdgeInsets.only(
                top: 8.h,
                left: 10.w,
                right: 10.w,
                bottom: 4.h,
              ),
              alignment: FractionalOffset.topLeft,
              child: Text(
                dataTips.title!,
                style: TextStyle(
                  color: AppColor.nileBlue,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                left: 10.w,
                right: 10.w,
                bottom: 17.h,
              ),
              alignment: FractionalOffset.topLeft,
              child: Text(
                dataTips.excerpt != null? dataTips.excerpt!: "",
                maxLines: 2,
                style: TextStyle(
                  color: AppColor.slateGrey,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
