import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:motionlife/data/model/youtube/youtube_model.dart';

import '../../../config/api/endpoint.dart';
import '../../../utils/color.dart';

class BannerYoutube extends StatelessWidget {
  final DataYoutube dataYoutube;

  BannerYoutube(this.dataYoutube);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 157.w,
      height: 225.h,
      child: Card(
        clipBehavior: Clip.antiAliasWithSaveLayer,
        margin: EdgeInsets.only(
          left: 10.w,
          bottom: 20.w,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
            10.r,
          ),
        ),
        elevation: 4,
        child: Stack(children: [
          Image.network(
            Endpoint.baseUrl + dataYoutube.image!,
            height: double.infinity,
            width: double.infinity,
            fit: BoxFit.fill,
          ),
          Container(
            margin: EdgeInsets.only(
              bottom: 18.h,
            ),
            alignment: FractionalOffset.bottomCenter,
            child: Text(
              dataYoutube.title!,
              style: const TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ]),
      ),
    );
  }
}
