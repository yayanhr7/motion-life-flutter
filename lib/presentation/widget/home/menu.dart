import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:motionlife/utils/color.dart';

class Menu extends StatelessWidget {
  final String image;
  final String title;

  Menu(this.title, this.image);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Image.asset(
          image,
        ),
        SizedBox(
          height: 5.h,
        ),
        Text(
          title,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 12.sp,
            color: AppColor.nileBlue,
          ),
        ),
      ],
    );
  }
}
