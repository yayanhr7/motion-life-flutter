import 'package:flutter/material.dart';
import 'package:motionlife/utils/color.dart';

class Loading extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColor.loading,
      child: Center(
        child: CircularProgressIndicator(
          color: AppColor.bluish,
        ),
      ),
    );
  }
}
