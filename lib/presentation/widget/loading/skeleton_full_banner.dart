import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:skeletons/skeletons.dart';

class SkeletonFullBanner extends StatelessWidget {
  const SkeletonFullBanner({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 243.h,
      child: const SkeletonAvatar(
        style: SkeletonAvatarStyle(
          width: double.infinity,
        ),
      ),
    );
  }
}
