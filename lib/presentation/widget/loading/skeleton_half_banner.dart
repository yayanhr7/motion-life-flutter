import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:motionlife/presentation/widget/loading/skeleton_loading.dart';

class SkeletonHalfBanner extends StatelessWidget {
  const SkeletonHalfBanner({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 157.w,
      height: 157.h,
      child: Card(
        clipBehavior: Clip.antiAliasWithSaveLayer,
        margin: EdgeInsets.only(
          left: 10.w,
          bottom: 20.w,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
            10.r,
          ),
        ),
        elevation: 4,
        child: SkeletonAvatarLoading(
          157.w,
          157.h,
        ),
      ),
    );
  }
}
