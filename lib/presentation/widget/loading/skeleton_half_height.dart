import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:skeletons/skeletons.dart';

class SkeletonHalfHeight extends StatelessWidget {
  const SkeletonHalfHeight({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 157.w,
      height: 225.h,
      child: Card(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      margin: EdgeInsets.only(
        left: 10.w,
        bottom: 20.w,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(
          10.r,
        ),
      ),
      elevation: 4,
        child: const SkeletonAvatar(
          style: SkeletonAvatarStyle(
            width: double.infinity,
          ),
        ),
      ),
    );
  }
}
