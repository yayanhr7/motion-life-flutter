import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:skeletons/skeletons.dart';

class SkeletonAvatarLoading extends StatelessWidget {
  final double width;
  final double height;

  SkeletonAvatarLoading(this.width, this.height);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: const SkeletonAvatar(
        style: SkeletonAvatarStyle(
          height: double.infinity,
          width: double.infinity,
        ),
      ),
    );
  }
}
