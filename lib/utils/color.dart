import 'package:flutter/material.dart';

class AppColor{
  static Color primary = HexColor("#205C91");
  static Color pictonBlue = HexColor("#1E7DBB");
  static Color grey98 = HexColor("#fafafa");
  static Color bluish = HexColor("#1e7dbb");
  static Color grey58 = HexColor("#949494");
  static Color grey = HexColor("#808080");
  static Color loading = HexColor("#80000000");
  static Color crimson = HexColor("#DA1414");
  static Color greyWarm = HexColor("#4e4e4e");
  static Color slateGrey = HexColor("#7A869A");
  static Color linkWater = HexColor("#C1C7D0");
  static Color grey90 = HexColor("#E5E5E5");
  static Color nileBlue = HexColor("#172B4D");


}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}