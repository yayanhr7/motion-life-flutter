import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'color.dart';

class DefaultDot extends StatefulWidget {

  int currentIndex;
  int index;
  Color colors;

  DefaultDot(this.currentIndex, this.index, this.colors);

  @override
  State<DefaultDot> createState() => _DefaultDotState();
}

class _DefaultDotState extends State<DefaultDot> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 6.h,
      width: widget.currentIndex == widget.index ? 20.w : 6.w,
      margin: EdgeInsets.symmetric(horizontal: 3.w),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(3.r),
        color: widget.colors,
      ),
    );
  }
}
