import 'package:shared_preferences/shared_preferences.dart';

class Pref {

  static Future<bool> isFirstTime() async {
    var pref = await SharedPreferences.getInstance();
    bool? isFirst;
    if (pref.containsKey('first_time')) {
      isFirst = pref.getBool('first_time');
    }else{
      isFirst = true;
      await pref.setBool('first_time', false);
    }
    return isFirst!;
  }
}
