import 'package:flutter/material.dart';

class SocialIcon{
  static const IconData facebook = IconData(0xe255, fontFamily: 'MaterialIcons');
  static const IconData googlePlus = IconData(0xe902, fontFamily: "MaterialIcons");

}