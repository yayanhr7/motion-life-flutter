import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:motionlife/data/model/login/login.dart';

class UserPref {
  static var storage = const FlutterSecureStorage();

  static AndroidOptions _getAndroidOptions() => const AndroidOptions(
        encryptedSharedPreferences: true,
      );
  static var options =
      const IOSOptions(accessibility: IOSAccessibility.first_unlock);

  static void saveUserLogin(DataLogin dataLogin) async {
    try {
      await storage.write(
          key: 'user_data',
          value: jsonEncode(dataLogin.toJson()),
          iOptions: options,
          aOptions: _getAndroidOptions());
    } catch (e) {
      print(e.toString());
    }
  }

  static Future<DataLogin?> getUserLogin() async {
    try {
      var user = await storage.read(
          key: 'user_data', iOptions: options, aOptions: _getAndroidOptions());
      if (user?.isNotEmpty ?? false) {
        return DataLogin.fromJson(jsonDecode(user!));
      }
      return null;
    } catch (e) {
      print(e.toString());
    }
  }

  static removeUserLogin() async{
    await storage.delete(key: 'user_data');
  }
}
